import React, { Component } from 'react';
import './App.css';

 class App extends Component {
  // constructor (...args) {
  //   super(...args)
  // }

  constructor (props) {
    console.log('constructor')
    super(props)
    this.state = { mensajeInicial: 'Mensaje inicial' }
    // this.handleClick = this.handleClick.bind(this)
  }

  handleClick = () => {
  // handleClick () {
    this.setState({ mensajeInicial: 'Cambia el mensaje' })
  }

  render () {
    console.log('render')
    return (
      <div className="App">
        <h4>Ciclo de montaje</h4>
        {this.state.mensajeInicial}
        <button onClick={this.handleClick}>Cambiar mensaje!</button>
      </div>
    );
  }
}

export default App;
