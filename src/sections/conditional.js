import React, { Component } from 'react'

class LoginButton extends Component {
	render () {
		return <button>Iniciar sesión</button>
	}
}

class LogoutButton extends Component {
	render () {
		return (
			<div>
				<p>Bienvenido usuario</p>
				<button>Cerrar sesión</button>
			</div>
		)
	}
}

// function useConditionalRendering (mostrarA) {
// 	if (mostrarA) {
// 		return <LoginButton />
// 	}
// 	return <LogoutButton />
// }

export default class ConditionalSection extends Component {
	constructor () {
		super()
		this.state = {isUserLogged: true}
	}

	render () {
		// const conditionalComponent = useConditionalRendering(this.state.isUserLogged)
		// const conditionalComponent = this.state.isUserLogged ? <LoginButton /> : <LogoutButton />
		return (
			<div>
				<h4>Conditional rendering</h4>
				{this.state.isUserLogged ? <LoginButton /> : <LogoutButton />}
			</div>
		)
	}
}
