import React, { Component } from 'react';
import './App.css';


class Box extends Component {
  render () {
    return (
      <div
        style={{ border: '1px solid #000', margin: 5, pagging: 5 }}>
        {this.props.children}
      </div>
    )
  }
}

class Article extends Component {
  render () {
    return (
      <section>
        <h2>{this.props.title}</h2>
        <p><em>Escrito por: {this.props.author}</em></p>
        <Box>{this.props.date}</Box>
        <article>
          {this.props.children}
        </article>
      </section>
    )
  }
}

 class App extends Component {

  render () {
    return (
      <div className="App">
        <h4>Trabajando con children props</h4>
        <Article
          author='Jose'
          date={new Date().toLocaleDateString()}
          title='Artículo sobre props children'
        >
          <p>El contenido lo envolvemos dentro del componente Article será enviado como this.props.children</p>
          <strong>Y mantiene las etiquetas y componentes añadidos</strong>
        </Article>
      </div>
    );
  }
}

export default App;
