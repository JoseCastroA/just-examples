import React, { Component } from 'react';
// import cars from './data/cars.json'
import './App.css';

// class CarItem extends Component {
//   render () {
//     const { car } = this.props
//     return (
//       <li>
//         <p><strong>Nombre: </strong>{car.name}</p>
//         <p><strong>Nombre: </strong>{car.company}</p>
//       </li>
//     )
//   }
// }

 class App extends Component {

  constructor () {
    super()
    this.state = {mouseX: 0, mouseY: 0, inputName: '', inputTwitter: '@', inputTerms: true}
    // this.handleMouseMove = this.handleMouseMove.bind(this) // Enlazar componente a los métodos que controlan los eventos
  }

  handleMouseMove = (e) => {
    const { clientX, clientY } = e
    this.setState({ mouseX: clientX, mouseY: clientY  })
  }

  handleClick = (e) => {
    e.preventDefault()
    // const name = this.inputName.value
    // const email = this.inputTwitter.value

    // console.log({name, email})

    console.log(this.state)
  }

  handleSubmit = (e) => {
    e.preventDefault()
    const name = this.inputName.value
    const email = this.inputTwitter.value

    console.log({name, email})
  }

  handleChange = (e) => {
    console.log('handleChange', e.target.checked)
    this.setState({ inputTerms: e.target.checked })
  }

  render () {
    return (
      <div className="App">
        <h4>Trabajando con eventos</h4>
        {/* <button onClick={this.handleClick}>Hi there!</button> */}
        <div
          onMouseMove={this.handleMouseMove}
          style={{border: '1px solid #000', marginTop: 10, padding: 10}}>
          <p>{this.state.mouseX}, {this.state.mouseY}</p>
          </div>
        <br></br>
        <h4>Trabajando con formularios</h4>
        <form onSubmit={this.handleClick}>
          <p>
            <label htmlFor='name'>Nombre: </label>
            <input
              id='name'
              name='userName'
              onChange={e => this.setState({ inputName: e.target.value })}
              placeholder='Pon tu nombre'
              ref={inputElement => this.inputName = inputElement}
              value={this.state.inputName} />
          </p>

          <p>
            <label htmlFor='twitter'>Twitter: </label>
            <input
              id='twitter'
              name='twitterAccount'
              onChange={e => this.setState({ inputTwitter: e.target.value })}
              placeholder='Pon tu twitter'
              ref={inputElement => this.inputTwitter = inputElement}
              value={this.state.inputTwitter} />
          </p>

          <p>
            <label>
              <input
                checked={this.state.inputTerms}
                type='checkbox'
                onChange={this.handleChange} />
              Accepted terms
            </label>
          </p>

          <button
            // onClick={this.handleClick}
          >Enviar</button>
        </form>
      </div>
    );
  }
}

export default App;
