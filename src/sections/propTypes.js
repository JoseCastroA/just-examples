import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';

class Box extends Component {
  render () {
    return (
      <div
        style={{ border: '1px solid #000', margin: 5, pagging: 5 }}>
        {this.props.children}
      </div>
    )
  }
}

class Article extends Component {
  static propTypes = {
    author: PropTypes.string.isRequired
  }

  // constructor (props) {
  //   super(props)
  //   if (typeof props.author === 'undefined') {
  //     console.warn('Author prop is required')
  //     throw new Error('Author prop is required')
  //   }
  // }

  render () {
    const { author, children, date, title } = this.props
    return (
      <section>
        <h2>{title}</h2>
        {author && <p><em>Escrito por: {author}</em></p>}
        <Box>{date}</Box>
        <article>
          {children}
        </article>
      </section>
    )
  }
}

// Article.propTypes = {
//   author: PropTypes.string
// }

 class App extends Component {

  render () {
    return (
      <div className="App">
        <h4>Trabajando con children props and types</h4>
        <Article
          // author={true}
          date={new Date().toLocaleDateString()}
          title='Artículo sobre props children'
        >
          <p>El contenido lo envolvemos dentro del componente Article será enviado como this.props.children</p>
          <strong>Y mantiene las etiquetas y componentes añadidos</strong>
        </Article>
      </div>
    );
  }
}

export default App;
